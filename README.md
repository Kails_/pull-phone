## Index

* [Description](#Description)
* [Installation](#installation)
* [Usage](#usage)
* [License](#license)

## Description
Bash script that implements a basic file/folder filtering system in adb pull.

### Main Goal
Speeding up phone backup procedures.

### Infos
- There can be multiple device paths and filters options, but only one 
   destination path.
- Paths must be absolute.
- Paths that need to get filtered must be inside the device paths defined in 
   the options, so no multi level filtering.
   </br>
   Example:
   <pre>
             ----->D          We have folder A with inside folder B and C,
            /                 inside B there are D and F, if we want to filter 
      ----->B                 out D only, we need to add /A/B to filter path
     /      \                 ( -f "/A/B" ), and then add F to device paths  
    A        ----->F          ( -p "/A/B/F" ), this way we exclude D.
     \                        So the end result is:
      ----->C                   pull-phone -d ... -p "/A" -p "/A/B/F" -f "/A/B"
   </pre>

## Installation

### Requirements
- Bash Shell
- USB debug on your android phone

```sh
chmod 774 install.sh uninstall.sh && ./install.sh
```

## Usage
```sh
1) pull-phone -h
2) pull-phone -d "/home/<user>/backups/android_phone/storage" -p "/sdcard" -p "/sdcard/Android/media" -f "Android" -f "Music"
```

### How to config
1. Just use the options with arguments.

## License
Distributed under the ```BSD 2-Clause "Simplified"``` license.
